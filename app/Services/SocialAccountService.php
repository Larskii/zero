<?php

namespace App\Services;

use App\SocialAccount;
use App\User;

class SocialAccountService
{
    public function createOrGetUser($providerObj, $providerName)
    {
        
        $providerUser = $providerObj->user();
        if($providerName == 'vkontakte') {
            $providerUser->email = $providerUser->accessTokenResponseBody['email'] ?? null;
        }
        if (is_null($providerUser->getEmail())){
            return FALSE; 
        }

        $account = SocialAccount::whereProvider($providerName)
                        ->whereProviderUserId($providerUser->getId())
                            ->first();

        if ($account) {
            return $account->user;
        } else {
            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $providerName]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {
                $user = User::createBySocialProvider($providerUser);
            }

            $account->user()->associate($user);
            $account->save();

            return $user;

        }

    }
}