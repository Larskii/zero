<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Article;

class ArticlesController extends Controller
{
  public function index() {
  	$posts = Article::get();
  	
    return view('welcome', compact('posts'));
  }
}
