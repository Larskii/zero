<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Socialite;
use App\Services\SocialAccountService;

class SocialController extends Controller
{
  public function login($provider)
  {
      return Socialite::with($provider)->redirect();
  }

  public function callback(SocialAccountService $service, $provider)
  {

      $driver = Socialite::driver($provider);

      $user = $service->createOrGetUser($driver, $provider);
      if(empty($user)) {
        return redirect('/')->with(['error' => 'К учетной записи не привязан e-mail']);
      }
      \Auth::login($user, true);
      return redirect()->intended('/');

  }
}
